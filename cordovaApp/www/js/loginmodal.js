(function(){
	angular
		.module("cordovaApp")
		.service("LoginModal", LoginModal);

		LoginModal.$inject = ["$ionicModal", "$rootScope"];
		
		function LoginModal($ionicModal, $rootScope){
			var vm = this;

			vm.initialize = function($scope){
				var parent = $scope;
				var self = $scope || $rootScope.new();

				var promise = $ionicModal.
				fromTemplateUrl('templates/loginmodal.html', {
			      scope: self,
			      animation: 'slide-in-up'
			    }).then(function(modal){
			      self.modal = modal;
			      return modal;
			    });

			    self.closeMe = function(){
			    	self.modal.hide();
			    }

			    self.loginFacebook = function(){
			    	console.log("Facebook Login!");
			    }

			    return promise;
			}
		}
})();