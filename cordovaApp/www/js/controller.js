(function(){
	angular
		.module("cordovaApp")
		.controller("HomeCtrl", HomeCtrl);

		HomeCtrl.$inject = ["$ionicPlatform", "$http", "$state", "$scope", "LoginModal"];

		function HomeCtrl($ionicPlatform, $http, $state, $scope, LoginModal){
			var vm = this;

			vm.login = function(){
				LoginModal
					.initialize($scope)
					.then(function(mdl){
						mdl.show();
					});
			};

			$ionicPlatform.ready(function(){
				console.log("Entered!");
			});
		}
})();