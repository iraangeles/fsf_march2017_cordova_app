angular
	.module("starter")
	.controller("MyController", MyController);

MyController.$inject = ["$cordovaCamera", "$cordovaImagePicker", "$ionicPopup", "$state", "$ionicActionSheet", "$timeout", "$scope", "$ionicLoading", "LoginModal"]

function MyController($cordovaCamera, $cordovaImagePicker, $ionicPopup, $state, $ionicActionSheet, $timeout, $scope, $ionicLoading, LoginModal){
	var vm = this;
	
	vm.takePicture = function(){
		var options = {
			destinationType: Camera.DestinationType.FILE_URI,
			sourceType: Camera.PictureSourceType.CAMERA
		};

		$cordovaCamera
			.getPicture(options)
			.then(function(imageUri){
				vm.imageTaken = imageUri;
			}, function(err){
				//error do something
			});
	}

	vm.imageTaken = "";	
	vm.selectImage = function(){
		var options = {
			maximumImagesCount: 1,
			width: 800,
			height:800,
			quality: 80
		}

		$cordovaImagePicker
			.getPictures(options)
			.then(function(results){
				for(var i = 0; i < results.length; i++){
					vm.imageTaken = results[i];
				}
			}, function(error){
				//handle the error
			});
	}

	vm.users = [
		"User A",
		"User B",
		"User C"
	];

	vm.lists = [
		"List A",
		"List B",
		"List C",
		"List D",
		"List E",
		"List F"
	];

	vm.stillHaveData = true;
	vm.loadMoreDatas = function(){
		var nextList = vm.lists.length + 1;
		vm.lists.push("List " + nextList);
		if(vm.lists.length > 30){
			vm.stillHaveData = false;
		}

		$scope.$broadcast('scroll.infiniteScrollComplete');
	}


	vm.updateList = function(){
		//do nothing
		$timeout(function(){

			vm.users = [
				"User D",
				"User E",
				"User F"
			];

			//stop:
			$scope.$broadcast('scroll.refreshComplete');
		}, 5000);

	}

	vm.shareButton = function(){

		$ionicActionSheet.show({
			buttons: [
				{text: 'Facebook'},
				{text: 'Twitter'}
			],
			titleText: 'Share',
			cancelText: 'Cancel',
			destructiveText: 'Delete',
			cancel: function(){
				//do something
			},
			buttonClicked: function(index){
				if(index == 0){
					//some action
				}
				//do checking of index here
				return true;
			},
			destructiveButtonClicked: function(){
				//do something here
				return true;
			}
		});
	}

	vm.submitForm = function(){
		//begin
		$ionicLoading.show({
			template: '<ion-spinner icon="android"></ion-spinner>'
		});

		$timeout(function(){

			$ionicLoading.hide();

		}, 5000);
	}

	vm.showLogin = function(){
		LoginModal
			.initialized($scope)
			.then(function(mdl){
				mdl.show();
			});
	}

	vm.removeRecord = function(){
		var confirmation = $ionicPopup.confirm({
			title: 'Remove Records?',
			template: 'Remove this records?'
		});

		confirmation.then(function(res){
			if(res){
				//do remove
			}else{
				//do nothing
			}
		});
	}


}